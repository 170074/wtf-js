var myClarifaiApiKey = '62569dcb57b347df8031ed6a0a50bdee';
var myWolframAppId = 'RWU4HE-Q9UPPVXHQU';

var app = new Clarifai.App({apiKey: myClarifaiApiKey});

function loadFood(value, source) {
  var preview = document.getElementById('foodPic');
  var file    = document.querySelector("input[type=file]").files[0];
  var loader  = "https://media.giphy.com/media/3d3wqDz1TOoCKmZv6n/giphy.gif";
  var reader  = new FileReader();

  // load image
  reader.addEventListener("load", (e) => { 
    // change image to selected image
    preview.style.backgroundImage = 'url(' + reader.result + ')'
    analyseFood({ 
      base64: reader.result.split("base64,")[1] 
    });
  }, false);

  // play loader if an image was selected
  if (file) {
    reader.readAsDataURL(file);
    document.getElementById('foodInfo').innerHTML = '<img src="' + loader + '" class="loading" />';
  } else { 
    alert("No file selcted!"); 
  }
}

// get food name from Clarifai
function analyseFood(value) {
  app.models.predict(Clarifai.FOOD_MODEL, value).then(function(response) {
      if(response.rawData.outputs[0].data.hasOwnProperty("concepts")) {
        // set tag to identified food name
        var tag = response.rawData.outputs[0].data.concepts[0].name;

        // link to Wolfram
        var url = 'http://api.wolframalpha.com/v2/query?input='+tag+'%20nutrition%20facts&appid='+myWolframAppId;

        // post nutritional info
        getNutritionalInfo(url, result => {
          document.getElementById('foodInfo').innerHTML = '<p> You are eating:  '+ tag + '</p>' + "<img src='"+result+"'>";
        });
      }
    }, function(err) { console.log(err); }
  );
}